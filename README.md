# vue_ecommerce

I used the "Fake Store Api".
You can find user information to sign in and test the application at this address : https://fakestoreapi.com/docs 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
