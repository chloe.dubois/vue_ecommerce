import Vue from 'vue'
import VueRouter from 'vue-router'
import Homepage from '../views/Homepage.vue'
import store from '../store/index.js'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Homepage',
    component: Homepage
  },
  {
    path: '/product/:id',
    name: 'Product',
    component: () => import(/* webpackChunkName: "product" */ '../views/Product.vue'),
    props: true
  },
  {
    path: '/compte',
    name: 'Compte',
    meta: {
      auth: true
    },
    component: () => import(/* webpackChunkName: "compte" */ '../views/Compte.vue')
  },
  {
    path: '/panier',
    name: 'Panier',
    component: () => import(/* webpackChunkName: "about" */ '../views/Panier.vue')
  },
  {
    path: '/order',
    name: 'Order',
    meta: {
      auth: true
    },
    component: () => import(/* webpackChunkName: "order" */ '../views/Order.vue')
  },
  {
    path: '/connexion',
    name: 'Connexion',
    component: () => import(/* webpackChunkName: "connexion" */ '../views/Connexion.vue')
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.auth)) {
    if (!store.getters.isAuthenticated) {
      next({ name: 'Connexion' })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
